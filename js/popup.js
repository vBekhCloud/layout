$('[data-popup]').click(function() {
    openPopup();
});

$('.popup__overlay').click(function() {
    closePopup();
});

function openPopup() {
    var popup = $('.popup');
    var html = $('html');

    popup.addClass('active');
    html.css('overflow', 'hidden');
}

function closePopup() {
    var popup = $('.popup');
    var html = $('html');

    popup.removeClass('active');
    html.css('overflow', '');
}