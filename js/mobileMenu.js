$('[data-mobile-menu]').click(function() {
    openMobileMenu();
});

$('.mobile-menu__overlay').click(function() {
    closeMobileMenu();
});

function openMobileMenu() {
    var mobileMenu = $('.mobile-menu');
    var html = $('html');

    mobileMenu.addClass('active');
    html.css('overflow', 'hidden');
}

function closeMobileMenu() {
    var mobileMenu = $('.mobile-menu');
    var html = $('html');

    mobileMenu.removeClass('active');
    html.css('overflow', '');
}

$('.mobile-menu__dropdown').click(function() {
    $(this).addClass('active');
});

$('[data-mobile-menu-back]').click(function(event) {
    event.stopPropagation();

    $('.mobile-menu__dropdown.active').removeClass('active');
});