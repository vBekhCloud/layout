$('.footer__menu--dropdown').click(function() {
    var _this = $(this);
    var menuBody = _this.find('.footer__menu-body');
    menuBody.slideToggle(200);

    _this.toggleClass('open');
});