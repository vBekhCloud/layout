$('.side-menu__item-arrow').click(function() {
    var _this = $(this);
    var parentItem = _this.closest('.side-menu__item');
    _this.toggleClass('open');
    parentItem.find('.side-menu__child').slideToggle(200);
});